package com.solidgate.qa.implementations.api.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solidgate.qa.implementations.api.ApiClient;
import com.solidgate.qa.utils.SignatureGenerator;
import lombok.SneakyThrows;

import java.util.Map;

public class OrderHeaders {
    private final String merchant;
    private final String secret;
    private final ObjectMapper mapper;

    public OrderHeaders(String merchant, String secret) {
        this.merchant = merchant;
        this.secret = secret;
        this.mapper = ApiClient.getObjectMapper();
    }

    @SneakyThrows
    public Map<String, String> authHeadersForPostRequest(Object requestBody) {
        String jsonBody = mapper.writeValueAsString(requestBody);
        String signature = SignatureGenerator.generatePostSignature(merchant, jsonBody, secret);
        return authHeaders(signature);
    }

    public Map<String, String> authHeadersForGetRequest() {
        String signature = SignatureGenerator.generateGetSignature(merchant, secret);
        return authHeaders(signature);
    }

    private Map<String, String> authHeaders(String signature) {
        return Map.of(
                "merchant", merchant,
                "signature", signature
        );
    }
}
