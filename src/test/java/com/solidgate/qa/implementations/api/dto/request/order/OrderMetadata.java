package com.solidgate.qa.implementations.api.dto.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderMetadata {
    @JsonProperty("coupon_code")
    private String couponCode;
    @JsonProperty("partner_id")
    private String partnerId;
}
