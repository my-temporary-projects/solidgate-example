package com.solidgate.qa.implementations.api.dto.response.get_order;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardToken {
    @JsonProperty("token")
    private String token;
}
