package com.solidgate.qa.implementations.api.services;

import com.solidgate.qa.config.Service;
import com.solidgate.qa.implementations.api.ApiClient;
import com.solidgate.qa.implementations.api.dto.request.order.InitOrderRequest;
import com.solidgate.qa.implementations.api.dto.request.order.OrderStatusRequest;
import com.solidgate.qa.implementations.api.dto.response.get_order.GetOrderResponse;
import com.solidgate.qa.implementations.api.dto.response.init_order.InitOrderResponse;
import com.solidgate.qa.implementations.api.exceptions.ResponseException;
import io.qameta.allure.Step;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.Map;

public class OrderService {
    private final String merchant;
    private final String secret;

    public OrderService(String merchant, String secret) {
        this.merchant = merchant;
        this.secret = secret;
    }

    @Step("Send request to init order")
    @SneakyThrows(IOException.class)
    public InitOrderResponse initOrder(InitOrderRequest orderRequest) {
        OrderHeaders headers = new OrderHeaders(merchant, secret);
        Map<String, String> authHeaders = headers.authHeadersForPostRequest(orderRequest);
        ApiService apiService = ApiClient.getApiService(Service.PAYMENT_PAGE);
        Call<InitOrderResponse> call = apiService.initOrder(authHeaders, orderRequest);
        Response<InitOrderResponse> response = call.execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            throw new ResponseException("Error initiating order: ", response.errorBody());
        }
    }

    @Step("Send request to get order info")
    @SneakyThrows(IOException.class)
    public GetOrderResponse getOrderStatus(OrderStatusRequest orderStatusRequest) {
        OrderHeaders headers = new OrderHeaders(merchant, secret);
        Map<String, String> authHeaders = headers.authHeadersForPostRequest(orderStatusRequest);
        ApiService apiService = ApiClient.getApiService(Service.PAYMENT);
        Call<GetOrderResponse> call = apiService.getOrderStatus(authHeaders, orderStatusRequest);
        Response<GetOrderResponse> response = call.execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            throw new ResponseException("Error getting order status: " + response.errorBody());
        }
    }
}
