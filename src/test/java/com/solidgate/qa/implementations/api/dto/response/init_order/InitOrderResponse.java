package com.solidgate.qa.implementations.api.dto.response.init_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class InitOrderResponse {
    @JsonProperty("url")
    private String url;
    @JsonProperty("guid")
    private String guid;
}
