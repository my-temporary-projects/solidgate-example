package com.solidgate.qa.implementations.api.dto.response.get_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Transaction {
        @JsonProperty("id")
        private String id;
        @JsonProperty("created_at")
        private String createdAt;
        @JsonProperty("updated_at")
        private String updatedAt;
        @JsonProperty("amount")
        private Integer amount;
        @JsonProperty("currency")
        private String currency;
        @JsonProperty("marketing_amount")
        private Integer marketingAmount;
        @JsonProperty("marketing_currency")
        private String marketingCurrency;
        @JsonProperty("operation")
        private String operation;
        @JsonProperty("status")
        private String status;
        @JsonProperty("descriptor")
        private String descriptor;
        @JsonProperty("billing_details")
        private BillingDetails billingDetails;
        @JsonProperty("refund_reason")
        private String refundReason;
        @JsonProperty("refund_reason_code")
        private String refundReasonCode;
        @JsonProperty("card_token")
        private CardToken cardToken;
        @JsonProperty("card")
        private Card card;
        @JsonProperty("error")
        private Error error;
}
