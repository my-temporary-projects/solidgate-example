package com.solidgate.qa.implementations.api.services;

import com.solidgate.qa.implementations.api.dto.request.order.InitOrderRequest;
import com.solidgate.qa.implementations.api.dto.request.order.OrderStatusRequest;
import com.solidgate.qa.implementations.api.dto.response.get_order.GetOrderResponse;
import com.solidgate.qa.implementations.api.dto.response.init_order.InitOrderResponse;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;


public interface ApiService {

    @Headers({"Content-Type: application/json"})
    @POST("init")
    Call<InitOrderResponse> initOrder(@HeaderMap Map<String, String> headers, @Body InitOrderRequest orderRequest);

    @POST("status")
    Call<GetOrderResponse> getOrderStatus(@HeaderMap Map<String, String> headers, @Body OrderStatusRequest orderStatus);
}
