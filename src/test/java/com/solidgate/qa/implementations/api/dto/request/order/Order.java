package com.solidgate.qa.implementations.api.dto.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class Order {
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("order_description")
    private String orderDescription;
    @JsonProperty("order_items")
    private String orderItems;
    @JsonProperty("order_date")
    private String orderDate;
    @JsonProperty("order_number")
    private Integer orderNumber;
    @JsonProperty("type")
    private String type;
    @JsonProperty("settle_interval")
    private Integer settleInterval;
    @JsonProperty("retry_attempt")
    private Integer retryAttempt;
    @JsonProperty("force3ds")
    private Boolean force3ds;
    @JsonProperty("google_pay_allowed_auth_methods")
    private List<String> googlePayAllowedAuthMethods;
    @JsonProperty("customer_date_of_birth")
    private String customerDateOfBirth;
    @JsonProperty("customer_email")
    private String customerEmail;
    @JsonProperty("customer_first_name")
    private String customerFirstName;
    @JsonProperty("customer_last_name")
    private String customerLastName;
    @JsonProperty("customer_phone")
    private String customerPhone;
    @JsonProperty("traffic_source")
    private String trafficSource;
    @JsonProperty("transaction_source")
    private String transactionSource;
    @JsonProperty("purchase_country")
    private String purchaseCountry;
    @JsonProperty("geo_country")
    private String geoCountry;
    @JsonProperty("geo_city")
    private String geoCity;
    @JsonProperty("language")
    private String language;
    @JsonProperty("website")
    private String website;
    @JsonProperty("order_metadata")
    private OrderMetadata orderMetadata;
    @JsonProperty("success_url")
    private String successUrl;
    @JsonProperty("fail_url")
    private String failUrl;
}
