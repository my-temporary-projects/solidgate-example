package com.solidgate.qa.implementations.api.dto.response.get_order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Order {
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("order_description")
    private String orderDescription;
    @JsonProperty("subscription_id")
    private String subscriptionId;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("processing_amount")
    private Integer processingAmount;
    @JsonProperty("processing_currency")
    private String processingCurrency;
    @JsonProperty("marketing_amount")
    private Integer marketingAmount;
    @JsonProperty("marketing_currency")
    private String marketingCurrency;
    @JsonProperty("refunded_amount")
    private Integer refundedAmount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("customer_email")
    private String customerEmail;
    @JsonProperty("descriptor")
    private String descriptor;
    @JsonProperty("mid")
    private String mid;
    @JsonProperty("traffic_source")
    private String trafficSource;
    @JsonProperty("fraudulent")
    private Boolean fraudulent;
}