package com.solidgate.qa.implementations.api.dto.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderStatusRequest {
    @JsonProperty("order_id")
    private String orderId;
}
