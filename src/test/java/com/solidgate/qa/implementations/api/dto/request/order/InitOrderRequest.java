package com.solidgate.qa.implementations.api.dto.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class InitOrderRequest {
    @JsonProperty("order")
    private Order order;
    @JsonProperty("page_customization")
    private PageCustomization pageCustomization;
}
