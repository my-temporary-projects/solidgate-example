package com.solidgate.qa.implementations.api.dto.response.get_order;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Card {
    @JsonProperty("bank")
    private String bank;
    @JsonProperty("bin")
    private String bin;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("card_exp_month")
    private String cardExpMonth;
    @JsonProperty("card_exp_year")
    private Integer cardExpYear;
    @JsonProperty("card_holder")
    private String cardHolder;
    @JsonProperty("card_type")
    private String cardType;
    @JsonProperty("card_id")
    private String cardId;
    @JsonProperty("country")
    private String country;
    @JsonProperty("number")
    private String number;
    @JsonProperty("card_token")
    private CardToken cardToken;
}
