package com.solidgate.qa.implementations.api.dto.response.get_order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class GetOrderResponse {
    @JsonProperty("order")
    private Order order;
    @JsonProperty("transaction")
    private Transaction transaction;
    @JsonProperty("transactions")
    private Map<String, Transaction> transactions;

    private final Map<String, Object> additionalFields = new HashMap<>();

    @JsonAnySetter
    public void setAdditionalField(String name, Object value) {
        additionalFields.put(name, value);
    }

    // @JsonAnyGetter to retrieve the additional fields
    @JsonAnyGetter
    public Map<String, Object> getAdditionalFields() {
        return additionalFields;
    }
}
