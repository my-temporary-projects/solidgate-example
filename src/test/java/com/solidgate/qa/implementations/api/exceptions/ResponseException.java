package com.solidgate.qa.implementations.api.exceptions;

import lombok.NonNull;

public final class ResponseException extends RuntimeException {
    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(@NonNull String message, Object... params) {
        super(message.formatted(params));
    }
}