package com.solidgate.qa.implementations.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solidgate.qa.config.Service;
import com.solidgate.qa.implementations.api.services.ApiService;
import com.solidgate.qa.utils.LoggingInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.HashMap;
import java.util.Map;

public class ApiClient {
    private static final Map<Service, Retrofit> retrofitStorage = new HashMap<>();
    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public static Retrofit getClient(Service service) {
        if (!retrofitStorage.containsKey(service)) {
            LoggingInterceptor logging = new LoggingInterceptor();
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(service.getUrl())
                    .addConverterFactory(JacksonConverterFactory.create(mapper))
                    .build();
            retrofitStorage.put(service, retrofit);
        }
        return retrofitStorage.get(service);
    }

    public static ApiService getApiService(Service service) {
        return getClient(service).create(ApiService.class);
    }

    public static ObjectMapper getObjectMapper() {
        return mapper;
    }
}
