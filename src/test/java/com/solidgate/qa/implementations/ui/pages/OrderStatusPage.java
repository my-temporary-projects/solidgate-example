package com.solidgate.qa.implementations.ui.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$x;

public class OrderStatusPage extends BasePage {
    SelenideElement orderStatus = $x("//*[@data-testid='status-title']");

    @Step("Verify order status on the page")
    public void verifyOrderStatus(String expectedStatusText) {
        orderStatus.shouldHave(exactText(expectedStatusText));
    }

    @Override
    public void isCurrentlyDisplayed() {
        orderStatus.shouldBe(Condition.exist);
    }
}
