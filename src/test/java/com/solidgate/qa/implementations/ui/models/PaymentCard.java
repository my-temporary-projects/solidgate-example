package com.solidgate.qa.implementations.ui.models;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class PaymentCard {
    private String cardNumber;
    private String expiryDate;
    private String cvv;
    private String email;
}
