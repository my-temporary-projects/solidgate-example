package com.solidgate.qa.implementations.ui.pages;

import com.codeborne.selenide.SelenideElement;
import com.solidgate.qa.implementations.ui.models.PaymentCard;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class OrderPage extends BasePage {
    SelenideElement header = $x("//div[@data-testid='payment-method-header']");
    SelenideElement cardNumberInput = $x("//input[@data-testid='cardNumber']");
    SelenideElement cardExpiryDateInput = $x("//input[@data-testid='cardExpiryDate']");
    SelenideElement cardCvvInput = $x("//input[@data-testid='cardCvv']");
    SelenideElement emailInput = $x("//input[@data-testid='charity-email']");
    SelenideElement submitButton = $x("//button[@data-testid='submit']");

    @Step("Fill in card info fields with {0}")
    public OrderPage fillInCardInfo(PaymentCard card) {
        cardNumberInput.val(card.getCardNumber());
        cardExpiryDateInput.val(card.getExpiryDate());
        cardCvvInput.val(card.getCvv());
        emailInput.val(card.getEmail());
        return this;
    }

    @Step("Press submit button")
    public OrderStatusPage proceedOrder() {
        submitButton.shouldBe(enabled).click();
        return at(OrderStatusPage::new);
    }

    @Override
    public void isCurrentlyDisplayed() {
        header.shouldBe(visible).shouldHave(exactText("Pay with card"));
    }
}
