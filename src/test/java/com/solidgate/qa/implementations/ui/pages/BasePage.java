package com.solidgate.qa.implementations.ui.pages;

public abstract class BasePage implements PageInstantiator {
    public abstract void isCurrentlyDisplayed();
}
