package com.solidgate.qa.implementations.ui;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserSetup {
    public static void initDriver() {
        String browser = System.getProperty("browser").toLowerCase();
        Configuration.browser = browser;

        switch (browser) {
            case Browsers.CHROME -> {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--start-maximized");
                Configuration.browserCapabilities = options;
            }
            case Browsers.FIREFOX -> {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("moz:firefoxOptions",
                        new FirefoxOptions().addArguments("--start-maximized"));
                Configuration.browserCapabilities = capabilities;
            }
            default -> throw new IllegalArgumentException("Unsupported browser: " + browser);
        }
    }
}
