package com.solidgate.qa.implementations.ui.pages;

import java.util.function.Supplier;

public interface PageInstantiator {
    default <T extends BasePage> T at(Supplier<T> pageSupplier) {
        T page = pageSupplier.get();
        page.isCurrentlyDisplayed();
        return page;
    }
}
