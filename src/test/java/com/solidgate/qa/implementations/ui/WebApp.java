package com.solidgate.qa.implementations.ui;

import com.codeborne.selenide.Selenide;
import com.solidgate.qa.implementations.ui.pages.BasePage;
import com.solidgate.qa.implementations.ui.pages.PageInstantiator;
import io.qameta.allure.Step;

import java.util.function.Supplier;

public class WebApp implements PageInstantiator {
    @Step("Open page using URL {0}")
    public static <T extends BasePage> T open(String url, Supplier<T> pageSupplier) {
        Selenide.open(url);
        return new WebApp().at(pageSupplier);
    }
}
