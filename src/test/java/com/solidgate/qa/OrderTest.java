package com.solidgate.qa;

import com.solidgate.qa.assertions.OrderAssertions;
import com.solidgate.qa.config.Config;
import com.solidgate.qa.data.OrderStatus;
import com.solidgate.qa.data.OrderTestData;
import com.solidgate.qa.implementations.api.dto.request.order.InitOrderRequest;
import com.solidgate.qa.implementations.api.dto.request.order.OrderStatusRequest;
import com.solidgate.qa.implementations.api.dto.response.get_order.GetOrderResponse;
import com.solidgate.qa.implementations.api.dto.response.init_order.InitOrderResponse;
import com.solidgate.qa.implementations.api.services.OrderService;
import com.solidgate.qa.implementations.ui.BrowserSetup;
import com.solidgate.qa.implementations.ui.WebApp;
import com.solidgate.qa.implementations.ui.pages.OrderPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class OrderTest {
    private OrderService orderService;
    private InitOrderRequest initialOrderRequest;

    @BeforeClass
    public void setUp() {
        orderService = new OrderService(Config.MERCHANT_PK.get(), Config.MERCHANT_SK.get());
        initialOrderRequest = OrderTestData.orderWithMandatoryDataOnly();
    }

    @BeforeGroups(TestGroups.UI)
    public void initDriver() {
        BrowserSetup.initDriver();
    }

    @Test(groups = TestGroups.UI)
    public void testCreateAndSubmitOrder() {
        InitOrderResponse initOrderResponse = orderService.initOrder(initialOrderRequest);

        WebApp.open(initOrderResponse.getUrl(), OrderPage::new)
                .fillInCardInfo(OrderTestData.paymentCardForSuccessOrder())
                .proceedOrder()
                .verifyOrderStatus("Payment successful!");
    }

    @Test(dependsOnMethods = "testCreateAndSubmitOrder")
    public void testGetOrderStatus() {
        OrderStatusRequest orderStatusRequest = new OrderStatusRequest(initialOrderRequest.getOrder().getOrderId());
        GetOrderResponse orderStatusResponse = orderService.getOrderStatus(orderStatusRequest);
        new OrderAssertions(initialOrderRequest, orderStatusResponse)
                .verifyOrder(OrderStatus.APPROVED)
                .verifyTransactions()
                .complete();
    }
}
