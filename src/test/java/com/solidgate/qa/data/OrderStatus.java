package com.solidgate.qa.data;

public enum OrderStatus {
    CREATED,
    PROCESSING,
    SETTLE_PENDING,
    APPROVED,
    DECLINED,
    REFUNDED,
    AUTH_OK,
    AUTH_FAILED,
    SETTLE_OK,
    PARTIAL_SETTLED,
    VOID_OK
}
