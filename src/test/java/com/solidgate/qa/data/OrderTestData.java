package com.solidgate.qa.data;

import com.solidgate.qa.config.Config;
import com.solidgate.qa.implementations.api.dto.request.order.InitOrderRequest;
import com.solidgate.qa.implementations.api.dto.request.order.Order;
import com.solidgate.qa.implementations.api.dto.request.order.PageCustomization;
import com.solidgate.qa.implementations.ui.models.PaymentCard;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class OrderTestData {
    public static InitOrderRequest orderWithMandatoryDataOnly() {
        Order order = Order.builder()
                .orderId(UUID.randomUUID().toString())
                .amount(ThreadLocalRandom.current().nextInt(1, 10000))
                .currency("EUR")
                .orderDescription("Test AQA order at %s".formatted(ZonedDateTime.now(ZoneId.of("UTC"))))
                .build();
        PageCustomization pageCustomization = PageCustomization.builder()
                .publicName("Central Perk")
                .build();
        return new InitOrderRequest(order, pageCustomization);
    }

    public static PaymentCard paymentCardForSuccessOrder() {
        return PaymentCard.builder()
                .cardNumber(Config.CARD_NUMBER_SUCCESS.get())
                .expiryDate("12/34")
                .cvv("123")
                .email("aqa-test-example@solidgate.com")
                .build();
    }
}
