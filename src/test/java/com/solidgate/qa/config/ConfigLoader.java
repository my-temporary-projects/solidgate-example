package com.solidgate.qa.config;

import io.opentelemetry.sdk.autoconfigure.spi.ConfigurationException;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

public class ConfigLoader {
    private static final Properties PROPERTIES = new Properties();

    static {
        try {
            PROPERTIES.load(new InputStreamReader(
                    Objects.requireNonNull(
                            ConfigLoader.class.getClassLoader().getResourceAsStream("config.properties")),
                    StandardCharsets.UTF_8)
            );
        } catch (Throwable e) {
            throw new ExceptionInInitializerError("Failed to load config: %s".formatted(e.getMessage()));
        }
    }

    public static String get(String key) {
        return Optional
                .ofNullable(PROPERTIES.getProperty(key))
                .orElseThrow(() -> new ConfigurationException("Key %s was not found".formatted(key)));
    }
}
