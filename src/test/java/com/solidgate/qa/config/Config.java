package com.solidgate.qa.config;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Config {
    MERCHANT_PK("public.key"),
    MERCHANT_SK("secret.key"),
    CARD_NUMBER_SUCCESS("success-order.card.number");

    private final String key;

    public String get() {
        return ConfigLoader.get(key);
    }
}


