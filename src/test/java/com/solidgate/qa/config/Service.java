package com.solidgate.qa.config;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Service {
    PAYMENT_PAGE("payment-page.url"),
    PAYMENT("pay.url");

    private final String key;

    public String getUrl() {
        return ConfigLoader.get(key);
    }
}
