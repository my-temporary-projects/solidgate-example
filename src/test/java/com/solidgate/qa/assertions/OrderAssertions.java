package com.solidgate.qa.assertions;

import com.solidgate.qa.data.OrderStatus;
import com.solidgate.qa.implementations.api.dto.request.order.InitOrderRequest;
import com.solidgate.qa.implementations.api.dto.response.get_order.GetOrderResponse;
import org.assertj.core.api.SoftAssertions;

import java.util.Objects;

public class OrderAssertions {
    private final SoftAssertions softly;
    private final InitOrderRequest request;
    private final GetOrderResponse response;

    public OrderAssertions(InitOrderRequest request, GetOrderResponse response) {
        this.request = Objects.requireNonNull(request);
        this.response = Objects.requireNonNull(response);
        this.softly = new SoftAssertions();
    }

    public OrderAssertions verifyOrder(OrderStatus expectedStatus) {
        var requestOrder = request.getOrder();
        var responseOrder = response.getOrder();
        //we can add description for each assertion line using as() or withFailMessage()
        softly.assertThat(responseOrder).isNotNull();
        softly.assertThat(responseOrder.getOrderId()).isEqualTo(requestOrder.getOrderId());
        softly.assertThat(responseOrder.getAmount()).isEqualTo(requestOrder.getAmount());
        softly.assertThat(responseOrder.getCurrency()).isEqualTo(requestOrder.getCurrency());
        softly.assertThat(responseOrder.getStatus()).isEqualTo(expectedStatus.name().toLowerCase());
        return this;
    }

    public OrderAssertions verifyTransactions() {
        //here must be assertions for transactions
        return this;
    }

    public void complete() {
        softly.assertAll();
    }
}
