package com.solidgate.qa.utils;

import lombok.SneakyThrows;
import org.apache.commons.codec.digest.HmacUtils;

import java.util.Base64;

public class SignatureGenerator {
    public static String generatePostSignature(String publicKey, String jsonString, String secretKey) {
        String text = "%s%s%s".formatted(publicKey, jsonString, publicKey);
        return generateSignature(text, secretKey);
    }

    public static String generateGetSignature(String publicKey, String secretKey) {
        String text = "%s%s".formatted(publicKey, publicKey);
        return generateSignature(text, secretKey);

    }

    @SneakyThrows
    private static String generateSignature(String inputString, String secretKey) {
        String hexStr = new HmacUtils("HmacSHA512", secretKey).hmacHex(inputString);
        return Base64.getEncoder().encodeToString(hexStr.getBytes());
    }
}
