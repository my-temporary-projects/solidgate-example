package com.solidgate.qa.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.qameta.allure.Allure;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class LoggingInterceptor implements Interceptor {
    private final ObjectMapper objectMapper;

    public LoggingInterceptor() {
        this.objectMapper = new ObjectMapper();
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpLoggingInterceptor.Logger logger = HttpLoggingInterceptor.Logger.DEFAULT;

        String requestLog = request.url() + "\n" + request.headers();
        if (request.body() != null && isJsonMediaType(request.body().contentType())) {
            requestLog += "\n" + prettyPrintJson(bodyToString(request.body()));
        }

        logger.log("Sending request: %s\n".formatted(requestLog));

        Response response = chain.proceed(request);

        ResponseBody responseBody = response.peekBody(Long.MAX_VALUE);
        String responseBodyString = responseBody.string();
        String responseLog = ("%s\n%s\n%s").formatted(
                response.request().url(), response.headers(), prettyPrintJson(responseBodyString));

        logger.log("Received response: %s\n".formatted(responseLog));

        Allure.addAttachment("Request", requestLog);
        Allure.addAttachment("Response", responseLog);

        return response;
    }

    private boolean isJsonMediaType(MediaType mediaType) {
        return mediaType != null && "json".equalsIgnoreCase(mediaType.subtype());
    }

    private String bodyToString(final RequestBody body) throws IOException {
        Buffer buffer = new Buffer();
        if (body != null) {
            body.writeTo(buffer);
        }
        return buffer.readUtf8();
    }

    private String prettyPrintJson(String json) throws IOException {
        Object jsonObject = objectMapper.readValue(json, Object.class);
        ObjectWriter writer = objectMapper.writerWithDefaultPrettyPrinter();
        return writer.writeValueAsString(jsonObject);
    }
}
